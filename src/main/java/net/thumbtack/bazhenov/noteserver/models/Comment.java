package net.thumbtack.bazhenov.noteserver.models;

import java.util.UUID;

/**
 * Created by ajaxpro on 16.03.2017.
 */
public class Comment {

    public Comment(User author, String status, int noteVersion) {

        this.setCommentId(UUID.randomUUID());
        this.setAuthorId(author.getUserId());
        this.setAuthor(author);
        this.setNoteVersion(noteVersion);
        this.setStatus(status);

    }

    private UUID commentId;
    private UUID authorId;
    private User author;
    private int noteVersion;
    private String status;

    public UUID getCommentId() {
        return commentId;
    }

    public void setCommentId(UUID commentId) {
        this.commentId = commentId;
    }

    public UUID getAuthorId() {
        return authorId;
    }

    public void setAuthorId(UUID authorId) {
        this.authorId = authorId;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public int getNoteVersion() {
        return noteVersion;
    }

    public void setNoteVersion(int noteVersion) {
        this.noteVersion = noteVersion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
