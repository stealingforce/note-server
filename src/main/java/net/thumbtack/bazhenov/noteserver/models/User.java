package net.thumbtack.bazhenov.noteserver.models;

import java.util.List;
import java.util.UUID;

/**
 * Created by ajaxpro on 16.03.2017.
 */
public class User {

    public User(String username, String password) {

        this.setUserId(UUID.randomUUID());
        this.setUsername(username);
        this.setPassword(password);
        this.setNotes(null);
        this.setFollowers(null);
        this.setFollowing(null);
        this.setIgnored(null);

    }

    private UUID userId;
    private String username;
    private String password;

    private List<Note> notes;
    private List<User> followers;
    private List<User> following;
    private List<User> ignored;

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<User> getIgnored() {
        return ignored;
    }

    public void setIgnored(List<User> ignored) {
        this.ignored = ignored;
    }

    public List<User> getFollowing() {
        return following;
    }

    public void setFollowing(List<User> following) {
        this.following = following;
    }

    public List<User> getFollowers() {
        return followers;
    }

    public void setFollowers(List<User> followers) {
        this.followers = followers;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

}
