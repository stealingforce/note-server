package net.thumbtack.bazhenov.noteserver.models;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by ajaxpro on 16.03.2017.
 */
public class Note {

    public Note(User author, String title, String status) {

        this.setNoteId(UUID.randomUUID());
        this.setAuthorId(author.getUserId());
        this.setAuthor(author);
        this.setVersion(0);
        this.setTitle(title);
        this.setStatus(status);
        this.setRating(0);
        this.setComments(null);
        this.setVersions(null);

    }

    private UUID noteId;
    private UUID authorId;
    private User author;
    private int version;
    private String status;
    private String title;
    private int rating;

    private List<Comment> comments;
    private Map<Integer, String> versions;

    public UUID getNoteId() {
        return noteId;
    }

    public void setNoteId(UUID noteId) {
        this.noteId = noteId;
    }

    public UUID getAuthorId() {
        return authorId;
    }

    public void setAuthorId(UUID authorId) {
        this.authorId = authorId;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Map<Integer, String> getVersions() {
        return versions;
    }

    public void setVersions(Map<Integer, String> versions) {
        this.versions = versions;
    }
}
