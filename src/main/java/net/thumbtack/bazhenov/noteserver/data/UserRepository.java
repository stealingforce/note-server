package net.thumbtack.bazhenov.noteserver.data;

import net.thumbtack.bazhenov.noteserver.models.User;

import java.util.List;
import java.util.UUID;

/**
 * Created by ajaxpro on 16.03.2017.
 */
public interface UserRepository extends Repository<User> {

    void createFollower(String username, User follower);
    void deleteFollower(String username, User follower);

    User getBy(UUID id);
    User getBy(String username);

}