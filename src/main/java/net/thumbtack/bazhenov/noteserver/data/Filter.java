package net.thumbtack.bazhenov.noteserver.data;

/**
 * Created by ajaxpro on 16.03.2017.
 */
@FunctionalInterface
public interface Filter<T> {
    boolean filter(T item);
}
