package net.thumbtack.bazhenov.noteserver.data;

import net.thumbtack.bazhenov.noteserver.models.Note;

import java.util.List;
import java.util.UUID;

/**
 * Created by ajaxpro on 16.03.2017.
 */
public interface NoteRepository extends Repository<Note> {

    List<Note> getBy(UUID id, boolean includeComments);
    List<Note> getBy(String username, boolean includeComments);

}
