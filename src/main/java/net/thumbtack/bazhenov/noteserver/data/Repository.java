package net.thumbtack.bazhenov.noteserver.data;

import java.util.List;

/**
 * Created by ajaxpro on 16.03.2017.
 */
public interface Repository<T> {

    List<T> all();

    boolean any(Filter<T> predicate);

    int getCount();

    T create(T item);

    int delete(T item);

    int delete(Filter<T> predicate);

    T find(Object... keys);
    T find(Filter<T> predicate);

    List<T> findAll(Filter<T> predicate);
    List<T> findAll(Filter<T> predicate, int index, int size);

    int update(T item);

}